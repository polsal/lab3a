﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(IGrzej);
        public static Type I2 = typeof(ICiśnienie);
        public static Type I3 = typeof(IStan);

        public static Type Component = typeof(Ekspres);

        public delegate object GetInstance(object Ekspres);

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(dMixior);
        public static Type MixinFor = typeof(IGrzej);

        #endregion
    }
}
