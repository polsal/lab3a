﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class CzujnikCiśnienia : ICiśnienie
    {

        public void Ciśnienie()
        {
            Console.WriteLine("Jest wysokie ciśnienie");
        }


        public void ZaNiskieCiśnienie()
        {
            Console.WriteLine("Ciśnienie jest za niskie");
        }
    }
}
