﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Ekspres: IStan, IGrzej, ICiśnienie
    {
        delegate void Delegacik(Ekspres EkspresDoKawy);
        void Delegacja(Delegacik delegat)
        {
            if (delegat != null)
                delegat(this);
        }
        Grzałka grzałka;
        CzujnikCiśnienia czujnik;
        Pojemnik pojemnik;
        public Ekspres()
        {
            grzałka = new Grzałka();
            czujnik = new CzujnikCiśnienia();
            pojemnik = new Pojemnik();
        }

        public void robimy()
        {
            grzałka.grzej();
            grzałka.wyłaczSie();
            czujnik.Ciśnienie();
            czujnik.ZaNiskieCiśnienie();
            pojemnik.Stan();
            pojemnik.Błąd();
        }

        public void Stan()
        {
            throw new NotImplementedException();
        }

        public void Błąd()
        {
            throw new NotImplementedException();
        }

        public void grzej()
        {
            throw new NotImplementedException();
        }

        public void wyłaczSie()
        {
            throw new NotImplementedException();
        }

        public void Ciśnienie()
        {
            throw new NotImplementedException();
        }

        public void ZaNiskieCiśnienie()
        {
            throw new NotImplementedException();
        }
    }
}
